r-bioc-dupradar (1.36.0+ds-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 11 Jan 2025 20:39:48 +0100

r-bioc-dupradar (1.36.0+ds-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 26 Nov 2024 11:47:42 +0100

r-bioc-dupradar (1.34.0+ds-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 16 Aug 2024 16:24:31 +0200

r-bioc-dupradar (1.34.0+ds-1~0exp) experimental; urgency=low

  * Team upload.
  * New upstream version
  * Set upstream metadata fields: Archive.
  * d/tests/autopkgtest-pkg-r.conf: skip on 32-bit architectures.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 13 Jul 2024 16:39:00 +0200

r-bioc-dupradar (1.32.0+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 30 Nov 2023 07:54:38 +0100

r-bioc-dupradar (1.30.3+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 13 Oct 2023 15:27:01 +0200

r-bioc-dupradar (1.30.2+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 09 Oct 2023 10:12:31 +0200

r-bioc-dupradar (1.30.0+ds-2) unstable; urgency=medium

  * Team upload.
  * Deactivate "extra_restrictions=skip-not-installable"
  * Reduce piuparts noise in transitions (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 21 Aug 2023 16:43:59 +0200

r-bioc-dupradar (1.30.0+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Add debian/tests/autopkgtest-pkg-r.conf

 -- Andreas Tille <tille@debian.org>  Thu, 20 Jul 2023 22:42:03 +0200

r-bioc-dupradar (1.28.0+ds-1) unstable; urgency=medium

  * Team upload.
  * Mark package as not building on i386 for Salsa CI

 -- Andreas Tille <tille@debian.org>  Sun, 20 Nov 2022 17:11:19 +0100

r-bioc-dupradar (1.26.1+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 11 Jul 2022 13:44:58 +0200

r-bioc-dupradar (1.26.0+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 13 May 2022 11:38:55 +0200

r-bioc-dupradar (1.24.0+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.24.0+ds

 -- Nilesh Patra <nilesh@debian.org>  Wed, 24 Nov 2021 20:42:19 +0530

r-bioc-dupradar (1.22.0+ds-2) unstable; urgency=medium

  * Team upload.
  * Add skip-not-installable to autopkgtest-pkg-r.conf
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)
  * Disable reprotest

 -- Andreas Tille <tille@debian.org>  Fri, 03 Sep 2021 12:50:39 +0200

r-bioc-dupradar (1.20.0+ds-3) unstable; urgency=high

  * Team upload.
  * Skip Testsuite: autopkgtest-pkg-r since there is currently no way
    to specify skip-not-installable restriction

 -- Andreas Tille <tille@debian.org>  Tue, 09 Feb 2021 19:57:19 +0100

r-bioc-dupradar (1.20.0+ds-2) unstable; urgency=medium

  * Team upload.
  * Autopkgtest Restrictions: skip-not-installable
  * Standards-Version: 4.5.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 09 Feb 2021 13:53:49 +0100

r-bioc-dupradar (1.20.0+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Sun, 01 Nov 2020 17:26:21 +0100

r-bioc-dupradar (1.18.0+ds-2) unstable; urgency=medium

  * Team upload.
  * Test-Depends: r-bioc-dupradar

 -- Andreas Tille <tille@debian.org>  Thu, 29 Oct 2020 17:49:22 +0100

r-bioc-dupradar (1.18.0+ds-1) unstable; urgency=medium

  * Initial release (closes: #970097)

 -- Steffen Moeller <moeller@debian.org>  Fri, 11 Sep 2020 18:21:11 +0200
